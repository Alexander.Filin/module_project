<?php

use Illuminate\Support\Facades\Route;

use Modules\Feedback\Http\Controllers\FeedbackController;

Route::prefix('feedback')->group(function() {
    Route::get('/', [FeedbackController::class, 'index']);
});
